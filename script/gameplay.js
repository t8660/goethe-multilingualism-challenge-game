'use strict';

// begin game play with the end of the 3-2-1 timer
function beginGamePlay() {
        setTimeout(() => {
            gameplayBox.className = ''
            gameplayBox.classList.add('gameplay')
            gameplayBox.classList.add(difficulty + '-g')
            newRound(getShuffledElements(difficulty))  
        }, 250) 
}
 
// get random characters and the answer for the round
const getShuffledElements = (difficulty) => {
    if(difficulty === 'easy'){difficulty = 3}
    else if (difficulty === 'medium'){difficulty = 6}
    else {difficulty = 9}
    const shuffled = allLanguages.sort(() => 0.5 - Math.random()).slice(0, difficulty)
    const possibleAnswer = shuffled.filter(item => !previousAnswers.includes(item));
    const answer = possibleAnswer[Math.floor(Math.random() * possibleAnswer.length)]
    previousAnswers.push(answer)
    roundAnswer = answer
    // console.log(answer.lang)
    setupAudio(roundAnswer.lang)
    return shuffled
}
// add in the characters and rules for clicking the characters
const newRound = (shuffled) => {
    roundCountValue ++
    const characters = shuffled
    gameSelection.innerHTML = ''
    selectedCharacter = ''
    characters.forEach(character => {
        gameSelection.innerHTML += `
        <div class="col-4 animate__animated animate__bounceInUp">
            <div class="image-holder option">
                <div class="text-center">
                    <p>${character.lang}</p>
                </div>
                <button data-sound="click" class="btn btn-confirm d-none">Confirm</button>
                <img src="${character.img}" alt="">
            </div>
        </div>
        `
    })
    roundClock()
    const options = gameSelection.querySelectorAll('.option')
    options.forEach(option => (
        option.addEventListener('click', () => {
            removeFocus()
            option.classList.add('image-selected')
            const currentBtn = option.querySelector('button')
            currentBtn.classList.remove('d-none')
            currentBtn.addEventListener('click', () => {
                selectedCharacter = option.querySelector('p').innerText
                clearInterval(gameTimer)
                endRound()
            })
        })
    ))
    function removeFocus () {
        options.forEach(option => {
            option.classList.remove('image-selected')
            option.querySelector('button').classList.add('d-none')
        })
    }
}

const setupAudio = (audioId) => {
    gameAudio = document.querySelector(`#${audioId.toLowerCase()}`)
    gameAudio.currentTime = 0;
    gameAudio.play()
    gameAudioListener()
}

const gameAudioListener = () => {
    gameAudio.currentTime = 0
    setTimeout(() => {
        gameAudio.play()
    }, 200)
    spans.forEach(span => {
        span.classList.add('playing')
    })
    gameAudio.addEventListener('ended', () => {
        spans.forEach(span => {
            span.classList.remove('playing')
        })
    })
}

repeat.addEventListener('click', () => {
    gameAudioListener()
    repeat.classList.add('repeat-active')
    setTimeout(() => {repeat.classList.remove('repeat-active')}, 500)
})

// function for the timer
const roundClock = () => {
    gameTimer = setInterval(() => {
       timer.innerText = remainingTime + 's'
       remainingTime --
       if(remainingTime === -1){
           endRound()
           clearInterval(gameTimer)
       }
   }, 1000)
}

const endRound = () => {
    container04.classList.add('d-none')
    container05.classList.remove('d-none')
    clearInterval(gameTimer)
    gameAudio.pause()
    remainingTime = 30
    timer.innerText = 30 + 's'
    if(selectedCharacter === roundAnswer.lang){
        correct.classList.remove('d-none')
        score += 25
        actualWinnings.push(1)
    } else if (!selectedCharacter){
        timeout.classList.remove('d-none')
        actualWinnings.push(0)
    } else {
        incorrect.classList.remove('d-none')
        actualWinnings.push(0)
    }
    correctAnswer.forEach(answer => {
        answer.innerText = roundAnswer.lang
    })
    yourScore.innerText = score + '/100'
}

const nextRound = (button) => {
    setTimeout(() => {
        container05.classList.add('d-none')
        button.disabled = false
        
        if(roundCountValue === 4){
            container06.classList.remove('d-none')
            setResults()
        } else {
            container04.classList.remove('d-none')
            newRound(getShuffledElements(difficulty))
            roundCount.innerText = roundCountValue + '/4'
            correct.classList.add('d-none')
            incorrect.classList.add('d-none')
            timeout.classList.add('d-none')
        }
    }, 250)
    
}
