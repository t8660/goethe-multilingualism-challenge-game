function reset() {
    setTimeout(() => {
        resetBtn.classList.add('d-none')
        if(!container04.classList.contains('d-none')){
            endRound()
        }
        previousAnswers = []
        roundCountValue = 0
        score = 0
        remainingTime = 30
        actualWinnings = []
        playing = false;
        ears.forEach(ear => {
            ear.classList.add('d-none')
            ear.classList.remove('green')
        })
        diffOptions.forEach(item => {item.classList.remove('difficulty-selected')})
        correct.classList.add('d-none')
        incorrect.classList.add('d-none')
        timeout.classList.add('d-none')
        
        container01.classList.remove('d-none')
        container02.classList.add('d-none')
        container03.classList.add('d-none')
        container04.classList.add('d-none')
        container05.classList.add('d-none')
        container06.classList.add('d-none')
        
    }, 250)
}

function displayReset() {
    setTimeout(() => {
        resetBtn.classList.remove('d-none')
    }, 250)
}