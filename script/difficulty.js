'use strict';

diffOptions.forEach(option => {
    option.addEventListener('click', () => {
        let sound = document.querySelector(`#general`)
        sound.currentTime = 0;
        sound.play()
        difficulty = option.id;
        diffOptions.forEach(item => {item.classList.remove('difficulty-selected')})
        option.classList.add('difficulty-selected')
        diffGoBtn.classList.remove('d-none')
    })
})